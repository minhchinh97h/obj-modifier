import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here

        float[] arrayBuff = new float[buf.limit()];

        buf.get(arrayBuff);

        float maxX, minX, maxY, minY, maxZ, minZ, avgX, avgY, avgZ;
        maxX = minX = maxY = minY = maxZ = minZ = 0f;

        float[] x_array = getAllCoordinateFloatArray(arrayBuff, 0),
                y_array = getAllCoordinateFloatArray(arrayBuff, 1),
                z_array = getAllCoordinateFloatArray(arrayBuff, 2);

        //Find the average X between minX and maxX by finding minX and maxX
        avgX = GetAveragePoint(x_array, minX, maxX);

        //Find the average Y between minY and maxY by finding minY and maxY
        avgY = GetAveragePoint(y_array, minY, maxZ);

        //Find the average Z between minZ and maxZ by fiding minZ and maxZ
        avgZ = GetAveragePoint(z_array, minZ, maxZ);

        //Print average point
        System.out.println(avgX + " " + avgY + " " + avgZ);

        //After we found the center point (avgX, avgY, avgZ), we move it to the origin (0, 0, 0),
        //meaning subtract (Xn -Xavg), (Yn - Yavg), (Zn - Zavg)

        //Because x_array, y_array and z_array have the same length
        for(int i = 0; i < x_array.length; i++){
            x_array[i] -= avgX;
            y_array[i] -= avgY;
            z_array[i] -= avgZ;
        }

        //Initialize the result float array of centered points
        float[] result = new float[arrayBuff.length];
        int index = 0; //To keep track of x, y, z array indexes

        //Assign x, y, z values accordingly
        for(int x = 0, y = 1, z = 2; x < result.length && y < result.length && z < result.length ; x += 3, y += 3, z += 3 ){
            result[x] = x_array[index];
            result[y] = y_array[index];
            result[z] = z_array[index];

            index += 1;
        }

        //Print out final centered points with x,y,z coordinates
        for(int i = 0; i < result.length; i++){
            System.out.println(result[i]);
        }

        buf.clear();
        buf.put(result);
        buf.position(0);
    }

    static float[] getAllCoordinateFloatArray(float[] array, int startIndex){
        int index = 0; //To keep track of result's index

        //result array'length of one axis (x or y or z) is equal to one third of array's length
        //since array will consist of all x, y, z values accordingly
        float[] result = new float[array.length/3];

        //get x, y, z coordinate - increment of 3
        for(int i = startIndex; i < array.length; i += 3){
            result[index] = array[i];
            index += 1;
        }

        return result;
    }

    static float GetAveragePoint(float[] array, float min, float max){
        //Initialize min and max to be the first value of array
        min = array[0];
        max = min;
        for(int i = 0; i < array.length; i++){
            if(Float.compare(array[i], min) <= 0){
                min = array[i];
            }
            if(Float.compare(array[i], max) >= 0){
                max = array[i];
            }
        }
        return (min + max) / 2;
    }
}



